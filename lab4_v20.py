import sys


def main():

    rock = 'rock'
    scissors = 'scissors'
    paper = 'paper'
    check_number = 0

    while check_number == 0:
        player_one = input('input player one move: ')
        try:
            (player_one.lower() == rock.lower()) or (player_one.lower() == scissors.lower()) or (player_one.lower() == paper.lower())
        except ValueError:
            print('wrong move')
    while check_number == 0:
        player_two = input('input player two move: ')
        if (player_two.lower() == rock) or (player_two.lower() == scissors) or (player_two.lower() == paper):
            break
        print ('wrong move')
    while check_number == 0:
        player_three = input('input player three move: ')
        if (player_three.lower() == rock) or (player_three.lower() == scissors) or (player_three.lower() == paper):
            break
        print ('wrong move')

    if len(player_one) + len(player_two) + len(player_three) == 17:
        print('Impossible to determinate a winner')
        sys.exit(0)
    if len(player_one) == len(player_two) == len(player_three):
        print('Impossible to determinate a winner')
        sys.exit(0)

    if len(player_one) == 8 or len(player_two) == 8 or len(player_three) == 8:
        if len(player_two) == 4:
            player_two = int(-2)
        if len(player_three) == 4:
            player_three = int(-2)
        if len(player_one) == 4:
            player_one = int(-2)
    else:
        if len(player_two) == 4:
            player_two = int(2)
        else:
            player_two = int(0)
        if len(player_one) == 4:
            player_one = int(2)
        else:
            player_one = int(0)
        if len(player_three) == 5:
            player_three = int(0)
        else:
            player_three = int(0)

    if (player_one > player_two) and (player_one > player_three):
        print ('Player one won')
    if (player_two > player_one) and (player_two > player_three):
        print ('Player two won')
    if (player_three > player_two) and (player_three > player_one):
        print ('Player three won')
    else:
        print('Impossible to determinate a winner')

if __name__ == "__main__":
    sys.exit(main())